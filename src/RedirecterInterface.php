<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use Iterator;
use Psr\Http\Message\UriInterface;
use RuntimeException;
use Stringable;

/**
 * RedirecterInterface interface file.
 *
 * This interface makes every redirecter like an API and defines the way to
 * access it. A Redirecter may hide only one link, or multiple links, but they
 * must be given back in an iterator.
 *
 * @author Anastaszor
 */
interface RedirecterInterface extends Stringable
{
	
	/**
	 * Get whether this object is able to process the given url.
	 *
	 * @param ?UriInterface $sourceUrl
	 * @return boolean true if the url is acceptable
	 */
	public function accept(?UriInterface $sourceUrl) : bool;
	
	/**
	 * Gets all the redirection links that are to be found at the given url.
	 * This method throws a \RuntimeException if it is impossible to reach or
	 * parse the target document to find the hidden link.
	 *
	 * @param ?UriInterface $sourceUrl the page where the links are hidden
	 * @return Iterator<integer, UriInterface> the links that are discovered
	 * @throws RuntimeException if it fails to reach or parse the target
	 */
	public function getRedirections(?UriInterface $sourceUrl) : Iterator;
	
}
