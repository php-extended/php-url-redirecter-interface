<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use Stringable;

/**
 * RedirecterFactoryConfigurationInterface interface file.
 * 
 * This represents a configuration for a redirecter factory.
 * 
 * @author Anastaszor
 */
interface RedirecterFactoryConfigurationInterface extends Stringable
{
	
	/**
	 * Adds the given domain to the whitelist for redirecters.
	 * 
	 * @param string $domain
	 * @return static
	 */
	public function addToWhitelist(string $domain) : static;
	
	/**
	 * Gets whether the given domain is whitelisted.
	 * 
	 * @param string $domain
	 * @return boolean
	 */
	public function isWhitelisted(string $domain) : bool;
	
}
