<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use Stringable;

/**
 * RedirecterFactoryInterface interface file.
 * 
 * This class builds redirecters based on the given configuration.
 * 
 * @author Anastaszor
 */
interface RedirecterFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new url redirecter based on the given configuration.
	 * 
	 * @param ?RedirecterFactoryConfigurationInterface $configuration
	 * @return RedirecterInterface
	 */
	public function createRedirecter(?RedirecterFactoryConfigurationInterface $configuration) : RedirecterInterface;
	
}
